import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Elemento from './components/Elemento.vue'
import CrearEle from './components/CrearEle.vue'
import SinAsignar from './components/SinAsignar.vue'
import SolUsuario from './components/SolUsuario.vue'
import CrearSol from './components/CrearSol.vue'

const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/user/home',
        name: "home",
        component: Home
    },
    {
        path: '/user/elemento',
        name: "Elemento",
        component: Elemento
    },
    
    {
        path: '/user/celemento',
        name: "CrearEle",
        component: CrearEle
    },

    {
        path: '/user/sinasignar',
        name: "SinAsignar",
        component: SinAsignar
    },
    
    {
        path: '/user/SolUsuario',
        name: "SolUsuario",
        component: SolUsuario
    },

    {
        path: '/user/CrearSol',
        name: "CrearSol",
        component: CrearSol
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;